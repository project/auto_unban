<?php

namespace Drupal\auto_unban\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'auto_unban.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auto_unban_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['seconds'] = [
      '#type' => 'select',
      '#title' => $this->t('Initial unban period'),
      '#description' => $this->t('This will only affect IPs banned in the future.'),
      '#options' => [
        60 => $this->t('One minute'),
        600 => $this->t('Ten minutes'),
        1800 => $this->t('Half hour'),
        3600 => $this->t('One hour'),
        3600 * 6 => $this->t('6 hours'),
        3600 * 12 => $this->t('12 hours'),
        86400 => $this->t('One day'),
        86400 * 2 => $this->t('Two days'),
        86400 * 7 => $this->t('One week'),
      ],
      '#default_value' => $config->get('seconds'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      ->set('seconds', $form_state->getValue('seconds'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}