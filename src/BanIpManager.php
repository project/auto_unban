<?php

namespace Drupal\auto_unban;

use Drupal\ban\BanIpManager as DrupalBanIpManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;

/**
 * AutoUnban version of the Ban IP manager.
 */
class BanIpManager extends DrupalBanIpManager {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The number of seconds to ban an IP for.
   *
   * @var int
   */
  protected $seconds;

  /**
   * Constructs a BanIpManager object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection which will be used to check the IP against.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   */
  public function __construct(Connection $connection, TimeInterface $time, ConfigFactoryInterface $config) {
    parent::__construct($connection);
    $this->time = $time;
    $this->seconds = $config->get('auto_unban.settings')->get('seconds', 3600);
  }

  /**
   * {@inheritdoc}
   */
  public function isBanned($ip) {
    return (bool) $this->connection->query("SELECT * FROM {ban_ip} WHERE [ip] = :ip AND expires > :expires", [
      ':ip' => $ip,
      ':expires' => $this->time->getCurrentTime(),
    ])->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function banIp($ip, $attempts = NULL) {
    // Get the current ban record.
    $banned = $this->connection->select('ban_ip', 'b')
      ->fields('b', ['attempts', 'expires'])
      ->condition('ip', $ip)
      ->execute()
      ->fetchObject();

    // Set the ban expiration:
    // 1. The first time the IP is banned, set the expiration to the configured
    // number of seconds (defaults to an hour).
    // 2. If the IP has been banned before, and the ban has expired, double the
    // ban period.
    // 3. If the IP is still in their ban period, extend it by the previous ban
    // time by the same amount applied with the current ban (this will usually
    // not happen because banned user's are rejected quickly using core's ban
    // middleware).
    $now = $this->time->getCurrentTime();
    $attempts = $attempts ?? $banned->attempts ?? 0;
    $growth = (!$banned || $now > $banned->expires) ? $attempts++ : $attempts;
    $expires = $now + $this->seconds * pow(2, $growth);

    // Update the ban record.
    $this->connection->merge('ban_ip')
      ->key(['ip' => $ip])
      ->fields([
        'ip' => $ip,
        'expires' => $expires,
        'attempts' => $attempts,
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function unbanIp($ip) {
    // To unban an IP, set the expires to 0. This is only used by the UI,
    // because IPs are auto-unbanned when the ban expires.
    $this->connection->merge('ban_ip')
      ->key(['ip' => $ip])
      ->fields(['expires' => 0])
      ->execute();
  }

}
