<?php

namespace Drupal\auto_unban;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies the ban manager service.
 */
class AutoUnbanServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides language_manager class to test domain language negotiation.
    // Adds entity_type.manager service as an additional argument.

    // Note: it's safest to use hasDefinition() first, because getDefinition() will
    // throw an exception if the given service doesn't exist.
    if ($container->hasDefinition('ban.ip_manager')) {
      $definition = $container->getDefinition('ban.ip_manager');
      $definition->setClass('Drupal\auto_unban\BanIpManager')
        ->addArgument(new Reference('datetime.time'))
        ->addArgument(new Reference('config.factory'));
    }
  }

}
