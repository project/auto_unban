<?php

namespace Drupal\auto_unban\Commands;

use Drupal\ban\BanIpManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * Class BanCommands
 *
 * @package Drupal\auto_unban\Commands
 */
class BanCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The Ban Manager.
   *
   * @var \Drupal\ban\BanIpManagerInterface
   */
  protected $banManager;

  /**
   * The time interface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The date Formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\ban\BanIpManagerInterface $ban_manager
   *   The Ban Manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time interface.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date Formatter.
   */
  public function __construct(BanIpManagerInterface $ban_manager, TimeInterface $time, DateFormatterInterface $date_formatter) {
    $this->banManager = $ban_manager;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Ban an IP.
   *
   * @command auto_unban:ban
   * @aliases ban
   * @option permanent If set, then the ban is permanent.
   * @param $ip
   *   The IP address to ban.
   */
  public function ban($ip, $options = [
    'permanent' => self::OPT,
  ]) {
    // If a permanent ban is requested, just use a high "attempts" account.
    // Since the ban time is exponential, after a certain number of attempts
    // all bans are practically permanent. Anything higher then 16 and we'd
    // start hitting the year 2038 integer size bug.
    $this->banManager->banIp($ip, !empty($options['permanent']) ? 16 : NULL);
  }

  /**
   * Un-ban an IP.
   *
   * @command auto_unban:unban
   * @aliases unban
   * @param $ip
   *   The IP address to unban.
   */
  public function unban($ip) {
    $this->banManager->unbanIp($ip);
  }

  /**
   * List banned IPs.
   *
   * @command auto_unban:banned
   * @aliases banned
   * @option limit The number of items to return.
   * @option sort The sort field, either 'expires' or 'attempts'.
   * @option ip The IP address to return.
   * @option all Show all banned entries (up to the limit), not just un-expired.
   *
   * @format json
   */
  public function banned($options = [
    'limit' => self::OPT,
    'sort' => self::OPT,
    'ip' => self::OPT,
    'all' => self::OPT,
  ]) {
    $query = \Drupal::database()->select('ban_ip', 'b')
      ->fields('b')
      ->orderBy($options['sort'] == 'expires' ? 'expires' : 'attempts', 'DESC');
    if ($options['ip']) {
      $query->condition('ip', $options['ip']);
    }
    if ($options['limit']) {
      $query->range(0, $options['limit']);
    }
    $now = $this->time->getCurrentTime();
    if (!$options['all']) {
      $query->condition('expires', $now, '>=');
    }
    $results = $query
      ->execute()
      ->fetchAllAssoc('ip');

    foreach ($results as $result) {
      $result->expiresFormatted = $this->dateFormatter->format($result->expires);
      if ($options['all']) {
        $result->expired = $now > $result->expires;
      }
    }

    return $results;
  }

}
