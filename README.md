This augments core's ban module to automatic unban IP's after a period of time. 

See https://www.drupal.org/project/auto_unban for further instructions.
